export enum PredefinedLevels {
  EASY = 'EASY',
  MEDIUM = 'MEDIUM',
  HARD = 'HARD',
  CUSTOM = 'CUSTOM',
}
