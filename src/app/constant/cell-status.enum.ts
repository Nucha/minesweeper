export enum CellStatus {
  COVERED = 'COVERED',
  CLEAR = 'CLEAR',
  FLAG = 'FLAG',
}
