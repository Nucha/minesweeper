export enum GameStatus {
  PLAYING = 'PLAYING',
  WIN = 'WIN',
  LOSE = 'LOSE',
}
