import { PredefinedLevels } from '../constant/predefined-levels.enum';

export class Difficulty {
  level: PredefinedLevels;
  columns: number | undefined;
  rows: number | undefined;
  mines: number | undefined;

  constructor(
    level: PredefinedLevels,
    columns?: number,
    rows?: number,
    mines?: number
  ) {
    this.level = level;
    if (level === PredefinedLevels.EASY) {
      this.columns = 6;
      this.rows = 6;
      this.mines = 6;
    } else if (level === PredefinedLevels.MEDIUM) {
      this.columns = 8;
      this.rows = 8;
      this.mines = 15;
    } else if (level === PredefinedLevels.HARD) {
      this.columns = 10;
      this.rows = 10;
      this.mines = 25;
    } else if (level === PredefinedLevels.CUSTOM) {
      this.columns = columns;
      this.rows = rows;
      this.mines = mines;
    }
  }
}
