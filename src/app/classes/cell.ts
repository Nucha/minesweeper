import { CellStatus } from '../constant/cell-status.enum';

export class Cell {
  status: CellStatus = CellStatus.COVERED;
  mine = false;
  proximityMines = 0;

  constructor(public row: number, public column: number) {}
}
