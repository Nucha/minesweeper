import { Cell } from './cell';
import { CellStatus } from '../constant/cell-status.enum';
import { GameStatus } from '../constant/game-status.enum';

const PEERS = [
  [-1, -1],
  [-1, 0],
  [-1, 1],
  [0, -1],
  [0, 1],
  [1, -1],
  [1, 0],
  [1, 1],
];

export class Board {
  cells: Cell[][] = [];
  private remainingCells = 0;
  private mineCount = 0;

  constructor(columns: number, rows: number, mines: number) {
    for (let y = 0; y < rows; y++) {
      this.cells[y] = [];
      for (let x = 0; x < columns; x++) {
        this.cells[y][x] = new Cell(y, x);
      }
    }

    // Assign mines
    for (let i = 0; i < mines; i++) {
      this.getRandomCell().mine = true;
    }

    // Count mines
    for (let y = 0; y < rows; y++) {
      for (let x = 0; x < columns; x++) {
        let adjacentMines = 0;
        for (const peer of PEERS) {
          if (
            this.cells[y + peer[0]] &&
            this.cells[y + peer[0]][x + peer[1]] &&
            this.cells[y + peer[0]][x + peer[1]].mine
          ) {
            adjacentMines++;
          }
        }
        this.cells[y][x].proximityMines = adjacentMines;

        if (this.cells[y][x].mine) {
          this.mineCount++;
        }
      }
    }
    this.remainingCells = rows * columns - this.mineCount;
  }

  getRandomCell(): Cell {
    const y = Math.floor(Math.random() * this.cells.length);
    const x = Math.floor(Math.random() * this.cells[y].length);
    return this.cells[y][x];
  }

  checkCell(cell: Cell): GameStatus {
    if (cell.status !== CellStatus.COVERED) {
      return GameStatus.PLAYING;
    } else if (cell.mine) {
      this.revealAll();
      return GameStatus.LOSE;
    } else {
      cell.status = CellStatus.CLEAR;

      // Empty cell, let's clear the whole block.
      if (cell.proximityMines === 0) {
        for (const peer of PEERS) {
          if (
            this.cells[cell.row + peer[0]] &&
            this.cells[cell.row + peer[0]][cell.column + peer[1]]
          ) {
            this.checkCell(
              this.cells[cell.row + peer[0]][cell.column + peer[1]]
            );
          }
        }
      }

      if (this.remainingCells-- <= 1) {
        return GameStatus.WIN;
      }
      return GameStatus.PLAYING;
    }
  }

  revealAll(): void {
    for (const row of this.cells) {
      for (const cell of row) {
        if (cell.status === CellStatus.COVERED) {
          cell.status = CellStatus.CLEAR;
        }
      }
    }
  }
}
