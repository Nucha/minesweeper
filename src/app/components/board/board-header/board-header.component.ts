import { Component, Input, OnInit } from '@angular/core';
import { PredefinedLevels } from '../../../constant/predefined-levels.enum';
import { GameStatus } from '../../../constant/game-status.enum';

@Component({
  selector: 'app-board-header',
  templateUrl: './board-header.component.html',
  styleUrls: ['./board-header.component.scss'],
})
export class BoardHeaderComponent implements OnInit {
  @Input() flags: number;
  @Input() level: PredefinedLevels;
  @Input() status: GameStatus;

  allowedStatus = GameStatus;

  constructor() {}

  ngOnInit(): void {}
}
