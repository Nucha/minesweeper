import { Component, OnInit } from '@angular/core';
import { Board } from '../../classes/board';
import { CellStatus } from '../../constant/cell-status.enum';
import { Cell } from '../../classes/cell';
import { LevelService } from '../../services/level.service';
import { GameStatus } from '../../constant/game-status.enum';
import { PredefinedLevels } from '../../constant/predefined-levels.enum';
import { ScoresService } from '../../services/scores.service';
import { Difficulty } from '../../classes/difficulty';
import { TimerService } from '../../services/timer.service';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss'],
})
export class BoardComponent implements OnInit {
  board: Board;
  cellStatus = CellStatus;
  flags: number;
  level: Difficulty;
  status = GameStatus.PLAYING;

  constructor(
    private levelService: LevelService,
    private scoresService: ScoresService,
    private timerService: TimerService
  ) {}

  ngOnInit(): void {
    this.createBoard();
  }

  checkCell(cell: Cell): void {
    const result = this.board.checkCell(cell);
    if (result === GameStatus.LOSE || result === GameStatus.WIN) {
      this.timerService.stopTimer();
      this.status = result;
      const record = this.generateRecord(result);
      this.scoresService.addNewRecord(record);
    }
  }

  flag($event: MouseEvent, cell: Cell): void {
    $event.preventDefault();
    if (cell.status !== CellStatus.CLEAR) {
      if (cell.status === CellStatus.FLAG) {
        cell.status = CellStatus.COVERED;
        this.flags = ++this.flags;
      } else if (cell.status === CellStatus.COVERED && this.flags > 0) {
        cell.status = CellStatus.FLAG;
        this.flags = --this.flags;
      }
    }
  }

  private createBoard(): void {
    const level = this.levelService.getGameLevel();
    // @ts-ignore
    this.board = new Board(level.columns, level.rows, level.mines);
    // @ts-ignore
    this.flags = level.mines;
    this.level = level;
  }

  private generateRecord(result: GameStatus): any {
    const timer = this.timerService.getTimerData();
    return {
      startTime: timer.start,
      endTime: timer.end,
      difficulty: this.level.level,
      totalTime: timer.difference,
      status: result === GameStatus.WIN ? 'Won' : 'Lost',
    };
  }
}
