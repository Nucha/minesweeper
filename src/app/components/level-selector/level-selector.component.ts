import { Component, OnDestroy, OnInit } from '@angular/core';
import { PredefinedLevels } from '../../constant/predefined-levels.enum';
import { LevelService } from '../../services/level.service';
import { Difficulty } from '../../classes/difficulty';
import { TimerService } from '../../services/timer.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-level-selector',
  templateUrl: './level-selector.component.html',
  styleUrls: ['./level-selector.component.scss'],
})
export class LevelSelectorComponent implements OnInit, OnDestroy {
  options = [
    PredefinedLevels.EASY,
    PredefinedLevels.MEDIUM,
    PredefinedLevels.HARD,
    PredefinedLevels.CUSTOM,
  ];
  predefinedLevels = PredefinedLevels;
  level: Difficulty;
  private subscription = new Subscription();

  constructor(
    private levelService: LevelService,
    private timerService: TimerService
  ) {}

  ngOnInit(): void {
    this.subscribeToGameLevel();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  subscribeToGameLevel(): void {
    const subs = this.levelService
      .getGameLevel$()
      .subscribe((gameLevel) => (this.level = gameLevel));
    this.subscription.add(subs);
  }

  updateLevel(newLevel: PredefinedLevels): void {
    this.levelService.setGameLevel(newLevel);
  }

  startGame(): void {
    if (this.level.level === PredefinedLevels.CUSTOM) {
      this.levelService.setGameLevel(
        PredefinedLevels.CUSTOM,
        this.level.columns,
        this.level.rows,
        this.level.mines
      );
    }
    this.startTimer();
  }

  private startTimer(): void {
    this.timerService.startTimer();
  }
}
