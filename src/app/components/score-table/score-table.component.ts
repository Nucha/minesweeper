import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { ScoresService } from '../../services/scores.service';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-score-table',
  templateUrl: './score-table.component.html',
  styleUrls: ['./score-table.component.scss'],
})
export class ScoreTableComponent implements OnInit, AfterViewInit {
  scores: MatTableDataSource<any> = new MatTableDataSource();
  displayedColumns: string[] = [
    'startTime',
    'endTime',
    'difficulty',
    'totalTime',
    'status',
  ];
  dateFormat = 'MM/dd/YYYY hh:mm a';
  timeSpentFormat = 'mm:ss';

  @ViewChild(MatSort) sort: MatSort;

  constructor(private scoresService: ScoresService) {}

  ngAfterViewInit(): void {
    this.scores.sort = this.sort;
  }

  ngOnInit(): void {
    this.getScoreData();
  }

  private getScoreData(): void {
    const data = this.scoresService.getAllRecords();
    this.scores = new MatTableDataSource(data);
  }
}
