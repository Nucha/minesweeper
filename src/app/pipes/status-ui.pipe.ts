import { Pipe, PipeTransform } from '@angular/core';
import { GameStatus } from '../constant/game-status.enum';

@Pipe({ name: 'statusUi' })
export class StatusUiPipe implements PipeTransform {
  transform(value: GameStatus): string | undefined {
    if (value === GameStatus.WIN) {
      return 'you Won!!!';
    }
    if (value === GameStatus.LOSE) {
      return 'you lost!!!';
    }
    if (value === GameStatus.PLAYING) {
      return 'Playing...';
    }
    return;
  }
}
