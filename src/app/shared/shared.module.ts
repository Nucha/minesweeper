import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatRadioModule } from '@angular/material/radio';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';

const angularMaterial = [
  MatRadioModule,
  MatInputModule,
  MatIconModule,
  MatButtonModule,
  MatCardModule,
  MatToolbarModule,
  MatTableModule,
  MatSortModule,
];

@NgModule({
  declarations: [],
  imports: [CommonModule, ...angularMaterial],
  exports: [CommonModule, ...angularMaterial],
})
export class SharedModule {}
