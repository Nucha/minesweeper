import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { PredefinedLevels } from '../constant/predefined-levels.enum';
import { Difficulty } from '../classes/difficulty';

@Injectable({ providedIn: 'root' })
export class LevelService {
  private gameLevel$ = new BehaviorSubject<Difficulty>(
    new Difficulty(PredefinedLevels.EASY)
  );

  constructor() {}

  setGameLevel(
    level: PredefinedLevels,
    columns?: number,
    rows?: number,
    mines?: number
  ): void {
    if (level !== PredefinedLevels.CUSTOM) {
      this.gameLevel$.next(new Difficulty(level));
    } else {
      this.gameLevel$.next(new Difficulty(level, columns, rows, mines));
    }
  }

  getGameLevel(): Difficulty {
    return this.gameLevel$.getValue();
  }

  getGameLevel$(): Observable<Difficulty> {
    return this.gameLevel$.asObservable();
  }
}
