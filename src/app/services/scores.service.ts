import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class ScoresService {
  records: any[] = [];

  constructor() {
    this.initializeSession();
  }

  addNewRecord(newRecord: any): void {
    this.records.push(newRecord);
    sessionStorage.scores = JSON.stringify(this.records);
  }

  getAllRecords(): any[] {
    return this.records;
  }

  private initializeSession(): void {
    const data = sessionStorage.getItem('scores');
    if (data === null) {
      this.records = [];
      sessionStorage.setItem('scores', JSON.stringify(this.records));
    } else {
      this.records = JSON.parse(data);
    }
  }
}
