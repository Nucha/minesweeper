import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class TimerService {
  private startTime: Date;
  private endTime: Date;

  constructor() {}

  startTimer(): void {
    this.startTime = new Date();
  }

  stopTimer(): void {
    this.endTime = new Date();
  }

  getTimerData(): any {
    return {
      start: this.startTime,
      end: this.endTime,
      // @ts-ignore
      difference: this.endTime - this.startTime,
    };
  }
}
