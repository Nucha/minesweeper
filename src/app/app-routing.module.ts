import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LevelSelectorComponent } from './components/level-selector/level-selector.component';
import { BoardComponent } from './components/board/board.component';
import { ScoreTableComponent } from './components/score-table/score-table.component';

const routes: Routes = [
  { path: '', redirectTo: '/setup', pathMatch: 'full' },
  { path: 'setup', component: LevelSelectorComponent },
  { path: 'board', component: BoardComponent },
  { path: 'scores', component: ScoreTableComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
