import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BoardComponent } from './components/board/board.component';
import { LevelSelectorComponent } from './components/level-selector/level-selector.component';
import { ScoreTableComponent } from './components/score-table/score-table.component';
import { SharedModule } from './shared/shared.module';
import { FormsModule } from '@angular/forms';
import { BoardHeaderComponent } from './components/board/board-header/board-header.component';
import { StatusUiPipe } from './pipes/status-ui.pipe';
import { NavbarComponent } from './components/navbar/navbar.component';

const declaredComponents = [];

const importedModules = [
  BrowserModule,
  FormsModule,
  BrowserAnimationsModule,
  AppRoutingModule,
  SharedModule,
];

@NgModule({
  declarations: [
    AppComponent,
    BoardComponent,
    LevelSelectorComponent,
    ScoreTableComponent,
    BoardHeaderComponent,
    StatusUiPipe,
    NavbarComponent,
  ],
  imports: importedModules,
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
